<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<title>sample</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<div class="container-fluid">
		<div>
			<div class="alert alert-dark" role="alert">
				<div align="right">
					<a href="#" class="alert-link">${userInfo.name}さん</a> <a
						href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
				</div>
			</div>
			<center>
				<h1>ユーザ削除確認</h1>
			</center>
		</div>
	</div>
	<form action="UserDeleteServlet" method="post">
		<div class="col-md-6 offset-md-5">

			<div>

				<h6>
					ログインID: <a href="#" class="alert-link">${user.loginId}</a> <br>
					を本当に削除してもよろしいでしょうか。
				</h6>

			</div>
		</div>

		<div class="container">
			<div class="row justify-content-md-center">

				<div class="col col-lg-2">
					<a class="btn btn-light" href="UserListServlet">キャンセル</a>
				</div>

				<div class="col col-lg-2">
						<button type="submit" class="btn btn-light">OK</button>
						<input type="hidden" value="${user.id}" name="id">

				</div>
			</div>
		</div>

	</form>
	</body>
	</html>
