<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
        <title>title</title>
         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
    <div class="row">
     <div class="container">

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
    <div class="container-fluid">
    	<form class="form-signin" action="LoginServlet" method="post">
	<center>

          <h1>ログイン画面</h1>

            <h4>ログインID <input type="text" name= "loginId" style="width:200px;" class="form-control"></h4>

            <h4>パスワード <input type="password" name= "password" style="width:200px;" class="form-control"></h4>


            <button type="submit"  class="btn btn-light btn-lg">ログイン</button>

    	</form>
    </div>
    </div>
    </div>
    </center>
</body>
</html>

