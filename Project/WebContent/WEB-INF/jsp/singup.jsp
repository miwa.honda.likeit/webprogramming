<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
  <head>
    <title>sample</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
	<body>
		<div class="container-fluid">
			<div>
				<div class="alert alert-dark" role="alert">
            		<div align="right">
                		<a href="#" class="alert-link">${userInfo.name}さん</a>
                		<a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
            		</div>
				</div>
			</div>

			<c:if test="${errMsg != null}" >
	    		<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>

			<div class="col-md-5 offset-md-4">
				<h1>ユーザ新規登録</h1>
			</div>
		</div>

		<form class="form-signup" action="SignUpServlet" method="post">
				<div class="form-group">

						<div class="col-md-5 offset-md-4">
							<label for="loginId">ログインID</label>
							<input type="text" style="width:550px" class="inline" name="loginId">
						</div>
						<div class="col-md-5 offset-md-4">
							<label for="password">パスワード</label>
							<input type="password" style="width:550px" class="inline" name ="password">
						</div>
						<div class="col-md-5 offset-md-4">
							<label for="password">パスワード(確認)</label>
							<input type="password" style="width:550px" class="inline" name = "password2">
						</div>
						<div class="col-md-5 offset-md-4">
							<label for="name">ユーザ名</label>
							<input type="text" style="width:550px" class="inline" name = "name">
						</div>
						<div class="col-md-5 offset-md-4">
							<label for="birthdate">生 年 月 日</label>
							<input type="date" style="width:550px" class="inline" name = "birthdate">
						</div>
					</div>


				<button type="submit"  class="btn btn-light btn-lg">登録</button>

		</form>
		<a href="UserListServlet" class="navbar-link logout-link">戻る</a>


	</body>
</html>