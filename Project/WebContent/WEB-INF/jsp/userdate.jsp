<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<title>sample</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<div class="container-fluid">
		<div>
			<div class="alert alert-dark" role="alert">
				<div align="right">
					<a href="#" class="alert-link">${userInfo.name}さん</a>
					<a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
				</div>
			</div>
		</div>

		<center>
			<h1>ユーザ情報詳細参照</h1>
		</center>
	</div>



	<div class="form-group">
		<form class="form-userdate" action="/UserManagement/UserDetailServlet" mehtod="get">

			<div class="col-md-5 offset-md-4">
				<label for="loginId">ログインID</label>
				<label for="loginId" class="col-md-4 offset-md-2">${user.loginId}</label>
			</div>
			<div class="col-md-5 offset-md-4">
				<label for="name">ユ ー ザ 名</label>
				<label for="name" class="col-md-4 offset-md-2">${user.name}</label>
			</div>
			<div class="col-md-5 offset-md-4">
				<label for="birthDate">生 年 月 日</label>
				 <label for="birthDate" class="col-4 offset-md-2">${user.birthDate}</label>
			</div>
			<div class="col-md-5 offset-md-4">
				<label for="createDate">登 録 日 時</label>
				 <label for="createDate" class="col-4 offset-md-2">${user.createDate}</label>
			</div>
			<div class="col-md-5 offset-md-4">
				<label for="updateDate">更 新 日 時</label>
				 <label for="updateDate"class="col-4 offset-md-2">${user.updateDate}</label>
			</div>
		</form>

	</div>



	<a href="UserListServlet" class="navbar-link logout-link">戻る</a>


</body>
</html>