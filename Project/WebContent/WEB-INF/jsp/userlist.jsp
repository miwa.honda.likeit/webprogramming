<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<div>
		<div class="alert alert-dark" role="alert">
			<div align="right">
				<a href="#" class="alert-link">${userInfo.name}さん</a> <a
					href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
			</div>
		</div>

		<center>
			<h1>ユーザー覧</h1>
		</center>

		<div align="right">
			<a href="SignUpServlet" class="alert-link">新規登録</a>
		</div>
		<form class="form-userResearch" action="UserListServlet" method="post">
			<div class="form-group">
				<div class="col-md-5 offset-md-4">
					<label for="loginId">ログインID</label> <input type="text"
						style="width: 550px" class="inline" name="loginId">
				</div>
				<div class="col-md-5 offset-md-4">
					<label for="name">ユーザー名</label> <input type="text"
						style="width: 550px" class="inline" name="name">
				</div>
				<div class="col-md-5 offset-md-4">
					<label for="birthDate">生 年 月 日</label> <input type="date"
						style="width: 265px" class="inline" placeholder="年/月/日"
						name="birthdate">〜 <input type="date" style="width: 265px"
						class="inline" placeholder="年/月/日" name="birthdate2">
				</div>
			</div>


			<div class="col-md offset-md-8">
				<button type="submit" class="btn btn-light btn-lg">検索</button>
			</div>




			<hr width="90%">

			<table class="table table-bordered">
				<thead class="thead-dark">

					<tr>
						<th scope="col">ログインID</th>
						<th scope="col">ユーザ名</th>
						<th scope="col">生年月日</th>
						<th scope="col"></th>
					</tr>
				</thead>
				<tbody>
					<tr>

	<c:forEach var="user" items="${userList}">
			<tr>
				<td>${user.loginId}</td>
				<td>${user.name}</td>
				<td>${user.birthDate}</td>
						<!-- TODO 未実装；ログインボタンの表示制御を行う -->
				<td>
				<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>

				<c:if test="${userInfo.loginId == 'admin'}">
				<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
				<a class="btn btn-danger" href="UserDeleteServlet?id=${user.id}">削除</a>
				</c:if>

				<c:if test="${userInfo.loginId == user.loginId}">
				<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
				</c:if>


				</td>

			</tr>

	</c:forEach>

				</tbody>
			</table>
		</form>
	</div>

</body>
</html>