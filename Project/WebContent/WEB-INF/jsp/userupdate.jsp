<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<title>sample</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<div class="container-fluid">
		<div>
			<div class="alert alert-dark" role="alert">
				<div align="right">
					<a href="#" class="alert-link">${userInfo.name}さん</a> <a
						href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
				</div>
			</div>

			<center>
				<h1>ユーザ情報更新</h1>
			</center>
		</div>
	</div>


			<c:if test="${errMsg != null}" >
	    		<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>
	<form class="form-userupdate" action="UserUpdateServlet" method="post">
	<div class="form-group">


		<div class="container">

		<div class="col-md-5 offset-md-4">
			<label for="loginId">ログインID</label>
			 <label for="loginId" class="col-md-4 offset-md-2">${user.loginId}</label>
			 <input type="hidden" value="${user.loginId}" name="loginId">
		</div>
		<div class="col-md-5 offset-md-4">
			<label for="password">パスワード</label>
			 <input type="password" style="width:500px" class="inline" name ="password">
			 <input type="hidden" value="${user.password}" name="password">
		</div>
		<div class="col-md-5 offset-md-4">
			<label for="password">パスワード(確認)</label>
			<input type="password" style="width:500px" class="inline" name = "password2">
			<input type="hidden" value="${user.password}" name="password2">
		</div>
		<div class="col-md-5 offset-md-4">
			<label for="user_nm">ユーザ名</label>
			<input type="text" value="${user.name}" style="width:500px" class="inline" name = "name">

		</div>
		<div class="col-md-5 offset-md-4">
			<label for="birthday">生 年 月 日</label>

			<input type="date" value="${user.birthDate}" style="width:500px" class="inline" name = "birthdate">
		</div>
		</div>
		<button type="submit" class="btn btn-light btn-lg">更新</button>

	</div>

	</form>

	<a href="UserListServlet" class="navbar-link logout-link">戻る</a>


</body>
</html>