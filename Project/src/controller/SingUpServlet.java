package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

@WebServlet("/SignUpServlet")
public class SingUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public SingUpServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		HttpSession session = request.getSession();
		User u = (User)session.getAttribute("userInfo");
		if (u == null){

		  response.sendRedirect("LoginServlet");
		  return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/singup.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String Name = request.getParameter("name");
		String BirthDate = request.getParameter("birthdate");

		if (!password.equals(password2)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/singup.jsp");
			dispatcher.forward(request, response);
			return;

		} else if (loginId.equals("") || password.equals("") || password2.equals("") || Name.equals("")|| BirthDate.equals("")){
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/singup.jsp");
			dispatcher.forward(request, response);
			return;
		}

		UserDao userDao = new UserDao();
		User rs = userDao.findByLoginInfo(loginId);
			if(rs != null) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/singup.jsp");
				dispatcher.forward(request, response);
				return;
			}



		try {
			userDao.singUp(loginId, password, Name, BirthDate);
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		response.sendRedirect("UserListServlet");

		}
	}


